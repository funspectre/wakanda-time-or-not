# Wakanda Time or Not

This is simple HTML page that uses a little JavaScript magic to tell the time in [Tanzania] (EAT / UTC+3) which is in the same timezone Wakanda would be in if it existed.

This project was prototyped using [Figma].

[Tanzania]: https://en.wikipedia.org/wiki/Tanzania
[Figma]: https://www.figma.com/proto/e2YKxcKnIb2GD6rdHmEE7CpA/Wakanda-or-Not

